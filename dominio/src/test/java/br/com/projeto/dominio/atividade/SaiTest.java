package br.com.projeto.dominio.atividade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SaiTest {

    @Test
    @DisplayName("Sai com funcao nula")
    void saiComFuncaoNula() {

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                        .contrato(0)
                        .entra(1)
                        .sai(null)
        );

    }

    @Test
    @DisplayName("Sai com funcao nao nula e estado obstruido")
    void saiComFuncaoNaoNulaEEstadoObstruido() {

        final Integer sai = Atividade
                .contrato(0)
                .entra(1)
                .realizar(integer -> null)
                .sai(o -> 2);

        Assertions.assertEquals(0,sai);
    }

    @Test
    @DisplayName("Sai com funcao nao nula e estado finalizado")
    void saiComFuncaoNaoNulaEEstadoFinalizado() {

        final Integer valor = 55;

        final String sai = Atividade
                .contrato("0")
                .entra(valor)
                .realizar(integer -> null)
                .senao(String::valueOf)
                .sai(o -> "qualquer coisa");

        Assertions.assertEquals(String.valueOf(valor), sai );

    }

    @Test
    @DisplayName("Sai com funcao nao nula, estado fluivel e Resultado da aplicaçao da funcao Nula")
    void saiComFuncaoNaoNulaEstadoFluivelResultadoDaAplicacaoDaFuncaoNula() {

        final Integer sai = Atividade
                .contrato(0)
                .entra(1)
                .realizar(integer -> integer*2)
                .sai(integer -> null);

        Assertions.assertEquals(0,sai);

    }

}
