package br.com.projeto.dominio.transacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class PeriodoTest {

    @Test
    @DisplayName("Quando em dataInicio nula excessao eh levantada")
    void dataInicioNula() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> Periodo.entre(null, "07/03/1997"));

    }

    @Test
    @DisplayName("Quando em dataFim nula excessao eh levantada")
    void dataFimNula() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> Periodo.entre("15/01/1997", null));

    }

    @Test
    @DisplayName("Data entre periodos trocada")
    void dataEntrePeriodoTrocada() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> Periodo.entre("30/04/1998", "10/10/1990"));

    }

    @Test
    @DisplayName("Data valida")
    void dataValida() {

        Assertions.assertTrue(Periodo.dataValida("01/04/1964"));

    }

    @Test
    @DisplayName("Data invalida")
    void dataInvalida() {

        Assertions.assertFalse(Periodo.dataValida("Cara palida"));

    }


    @Test
    @DisplayName("Quando periodo eh criado a data:'12/2/1997' deve estar valida")
    void dataEntrePeriodoValida() {

        final Periodo periodo = Periodo.entre("15/01/1997", "07/03/1997");

        Assertions.assertTrue(periodo.dataEntrePeriodo(LocalDate.of(1997, 2, 12)));

    }

    @Test
    @DisplayName("Data entre periodos invalida")
    void dataEntrePeriodoInvalida() {

        final Periodo periodo = Periodo.entre("15/01/1997", "07/03/1997");

        Assertions.assertFalse(periodo.dataEntrePeriodo(LocalDate.now()));
    }

    @Test
    @DisplayName("Data entre periodos invalida")
    void dataEntrePeriodoIgaual() {

        final Periodo periodo = Periodo.entre("15/01/1997", "07/03/1997");

        Assertions.assertTrue(periodo.dataEntrePeriodo(MinhasDatas.stringParaLocalDate("15/01/1997")));
    }



    @Test
    @DisplayName("Data entre periodos igual data inicio")
    void dataEntrePeriodoDataInicio() {

        final Periodo periodo = Periodo.mock();

        Assertions.assertTrue(periodo.dataEntrePeriodo(LocalDate.now()));

    }

    @Test
    @DisplayName("Data entre periodos igual data fim ")
    void dataEntrePeriodoDataFimIgual() {

        final Periodo periodo = Periodo.mock();

        Assertions.assertTrue(periodo.dataEntrePeriodo(LocalDate.now().plusDays(1)));

    }

    @Test
    @DisplayName("Data de entrada vai ser menor que data inicio")
    void dataEntrePeridoDataMenorQueDataInicio() {

        final LocalDate now = LocalDate.now();
        final Periodo periodo = Periodo.entre(MinhasDatas.localDateParaString(now.plusYears(1)), MinhasDatas.localDateParaString(now.plusYears(2)));

        Assertions.assertFalse(periodo.dataEntrePeriodo(now));
    }


}