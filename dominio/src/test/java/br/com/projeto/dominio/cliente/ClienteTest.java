package br.com.projeto.dominio.cliente;

import br.com.projeto.dominio.conta.ContaBancaria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

public class ClienteTest {

    @Test
    @DisplayName( "Cliente com cpf Valido" )
    public void clienteComCpfValido(){
        Assertions.assertDoesNotThrow( Cliente::mock );
    }

    @Test
    @DisplayName( "Cliente com cpf Invalido" )
    public void clienteComCpfInvalido(){
        Assertions.assertThrows( IllegalArgumentException.class, () -> Cliente
                .construtor( Cliente.mock() )
                .comCpf("3763731601a")
        );

    }

    @Test
    @DisplayName( "Verifica se Cpf e Valido" )
    public void cpfValido(){
        final String cpf = "37637316017";
        Assertions.assertTrue( Cliente.cpfValido( cpf ) );
    }

    @Test
    @DisplayName( "Verifica se Cpf e Invalido" )
    public void cpfInvalido(){
        final String cpf = "3763731601a";
        Assertions.assertFalse( Cliente.cpfValido( cpf ) );
    }

    @Test
    @DisplayName( "Cliente esta habilitado" )
    public void clienteHabilitado(){

        final SituacaoCliente situacaoCliente = SituacaoCliente.ATIVO;

        Assertions.assertTrue( Cliente
                        .construtor()
                        .comSituacaoCliente( situacaoCliente )
                        .construir()
                        .habilitado()
        );

    }

    @Test
    @DisplayName( "Cliente pendente" )
    public void clientePendente(){

        final SituacaoCliente situacaoCliente = SituacaoCliente.PENDENTE;

        Assertions.assertFalse( Cliente
                .construtor()
                .comSituacaoCliente( situacaoCliente )
                .construir()
                .habilitado()
        );

    }

    @Test
    @DisplayName( "Cliente cancelado" )
    public void clienteCancelado(){

        final SituacaoCliente situacaoCliente = SituacaoCliente.CANCELADO;

        Assertions.assertFalse( Cliente
                .construtor()
                .comSituacaoCliente( situacaoCliente )
                .construir()
                .habilitado()
        );

    }

    @Test
    @DisplayName( "Encontrar uma conta bancaria" )
    public void encontrarContaBancaria(){

        final Optional<ContaBancaria> optionalContaBancaria = Cliente
                .mock()
                .contaBancaria("0000", "00000");

        Assertions.assertTrue(optionalContaBancaria.isPresent());

        final ContaBancaria contaBancaria = optionalContaBancaria.get();

        Assertions.assertEquals("0000", contaBancaria.obterAgencia());

        Assertions.assertEquals("00000",contaBancaria.obterConta());

    }

}
