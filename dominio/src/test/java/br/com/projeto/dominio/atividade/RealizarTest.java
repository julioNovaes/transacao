package br.com.projeto.dominio.atividade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

public class RealizarTest {

    @Test
    @DisplayName("Realizar com Function<> nula")
    void realizarComFunctionNula(){

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                        .contrato(0L )
                        .entra(1 )
                        .realizar( (Function<? super Integer, Object>) null )
        );

    }

    @Test
    @DisplayName("Realizar com BiFunction<> nula")
    void realizarComBiFunctionNula(){

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                        .contrato(0L )
                        .entra(1 )
                        .realizar( (BiFunction<? super Integer, ? super Integer, Object>) null)
        );

    }

    @Test
    @DisplayName("Realizar com Function<> nao nula e estado obstruido")
    void realizarComFunctionNaoNulaEEstadoObstruido() {

        final String realizar = Atividade
                .contrato("1")
                .entra(2)
                .realizar(integer -> null)
                .realizar(o -> o)
                .sai(o -> "nulo");

        Assertions.assertEquals("1", realizar);

    }

    @Test
    @DisplayName("Realizar com Function<> nao nula e estado fluido")
    void realizarComFunctionNaoNulaEEstadoFluido() {

        final Long sai = Atividade
                .contrato(0L)
                .entra(1)
                .realizar(integer -> integer * 2)
                .sai(Long::valueOf);

        Assertions.assertEquals(2L, sai);

    }

    @Test
    @DisplayName("Realizar com BiFunction<> nao nula e estado obstruido")
    void realizarComBiFunctionNaoNulaEEstadoObstruido() {

        final String realizar = Atividade
                .contrato("1")
                .entra(2)
                .realizar( (a, b) -> null )
                .realizar(Integer::equals)
                .sai( o -> "nulo" );

        Assertions.assertEquals("1", realizar);

    }

    @Test
    @DisplayName("Realizar com funcao nao nula e estado Fluivel")
    void realizarComBiFunctionNaoNulaEEstadoFluivel() {

        final Long sai = Atividade
                .contrato(0L)
                .entra(1)
                .realizar((a, b) -> "10")
                .sai(Long::valueOf);

        Assertions.assertEquals(10, sai);

    }

}
