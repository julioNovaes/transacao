package br.com.projeto.dominio.atividade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GuardaTest {

    @Test
    @DisplayName("Guarda com predicado nulo")
    void guardaComPredicadoNulo() {

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                        .contrato("0")
                        .entra(1)
                        .guarda(null)
        );

    }

    @Test
    @DisplayName("Guarda com predicado nao nulo e estado obstruido")
    void guardaComPredicadoNaoNuloEEstadoObstruido() {

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .realizar(integer -> null)
                .guarda(integer -> true)
                .sai(Object::toString);

        Assertions.assertEquals("0",sai);

    }

    @Test
    @DisplayName("Guarda com predicado nao nulo e estado finalizado")
    void guardaComPredicadoNaoNuloEEstadoFinalizado() {

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .realizar(integer -> null)
                .senao(integer -> "1")
                .guarda(integer -> true)
                .sai(Object::toString);

        Assertions.assertEquals("1",sai);

    }

    @Test
    @DisplayName("Guarda com predicado nao nulo, estado nao finalizado e predicado verdadeiro")
    void guardaComPredicadoNaoNuloEstadoNaoFinalizadoEPredicadoVerdadeiro() {

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .guarda(integer -> true)
                .sai(Object::toString);

        Assertions.assertEquals("1",sai);
    }

    @Test
    @DisplayName("Guarda com predicado nao nulo, estado nao finalizado e predicado falso")
    void guardaComPredicadoNaoNuloEstadoNaoFinalizadoEPredicadoFalso() {

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .guarda(integer -> false)
                .sai(Object::toString);

        Assertions.assertEquals("0",sai);

    }

}
