package br.com.projeto.dominio.conta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ContaBancariaTest {

    @Test
    @DisplayName("Conta bancaria com dados de conta e agencia validos")
    void contaBancariaValida() {

        ContaBancaria contaBancaria = ContaBancaria.mock();
        Assertions.assertEquals("00000", contaBancaria.obterConta());
        Assertions.assertEquals("0000", contaBancaria.obterAgencia());

    }

    @Test
    @DisplayName("Conta bancaria com dados de agencia invalida")
    void contaBancariaAgenciaInvalida() {


        Assertions.assertThrows(IllegalArgumentException.class, () -> new ContaBancaria("000", null));


    }

    @Test
    @DisplayName("Conta bancaria com dados de conta invalida")
    void contaBancariaContaInvalida() {


        Assertions.assertThrows(IllegalArgumentException.class, () -> new ContaBancaria("0000", "00000000"));


    }

    @Test
    @DisplayName("Conta bancaria com dados de conta nula")
    void contaBancariaContaNula() {


        Assertions.assertThrows(IllegalArgumentException.class, () -> new ContaBancaria(null, "00000000"));


    }

    @Test
    @DisplayName("Conta bancaria com dados de agencia nula")
    void contaBancariaAgenciaNula() {


        Assertions.assertThrows(IllegalArgumentException.class, () -> new ContaBancaria("0000", null));


    }

    @Test
    @DisplayName("encontar conta bancaria valida")
    void encontarContaBancariaValida() {

        ContaBancaria contaBancaria = ContaBancaria.mock();
        Assertions.assertTrue(contaBancaria.encontrarContaBancaria(contaBancaria.obterAgencia(),contaBancaria.obterConta()));


    }

    @Test
    @DisplayName("encontar conta bancaria agencia invalida")
    void encontarContaBancariaAgenciaInvalida() {

        ContaBancaria contaBancaria = ContaBancaria.mock();
        Assertions.assertFalse(contaBancaria.encontrarContaBancaria("00",contaBancaria.obterConta()));


    }

    @Test
    @DisplayName("encontar conta bancaria conta invalida")
    void encontarContaBancariaContaInvalida() {

        ContaBancaria contaBancaria = ContaBancaria.mock();
        Assertions.assertFalse(contaBancaria.encontrarContaBancaria(contaBancaria.obterAgencia(),"cara palida"));


    }

    @Test
    @DisplayName("encontar conta bancaria agencia e conta invalida")
    void encontarContaBancariaAgenciaContaInvalida() {

        ContaBancaria contaBancaria = ContaBancaria.mock();
        Assertions.assertFalse(contaBancaria.encontrarContaBancaria("cara palida","cara palida"));


    }

}