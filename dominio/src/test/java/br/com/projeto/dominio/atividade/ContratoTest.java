package br.com.projeto.dominio.atividade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ContratoTest {

    @Test
    @DisplayName("Contrato Nulo")
    void contratoNulo(){
        Assertions.assertThrows( NullPointerException.class, () -> Atividade.contrato( null ) );
    }

    @Test
    @DisplayName("Entra Nulo")
    void entraNulo(){
        Assertions.assertThrows( NullPointerException.class, () -> Atividade.contrato(0).entra( null ) );
    }

}