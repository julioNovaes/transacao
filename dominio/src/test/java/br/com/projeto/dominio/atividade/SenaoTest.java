package br.com.projeto.dominio.atividade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

public class SenaoTest {

    @Test
    @DisplayName("Senao com Function<> nula")
    void senaoComFunctionNula(){

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                    .contrato("0")
                    .entra(1)
                    .senao((Function<? super Integer, ? extends String>) null)
        );

    }

    @Test
    @DisplayName("Senao com Function<> nao nula e estado fluivel")
    void senaoComFunctionNaoNulaEEstadoFluivel(){

        final String qualquerCoisa = "qualquer coisa";

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .realizar(integer -> integer * 2)
                .senao(integer -> "5")
                .sai(integer -> qualquerCoisa);

        Assertions.assertEquals(qualquerCoisa,sai);
    }
    @Test
    @DisplayName("Senao com Function<> nao nula, estado obstruido e Resultado da funcao Nula")
    void senaoComFunctionNaoNulaEstadoObstruidoEResultadoDaFuncaoNula(){

        final String contrato = "0";

        final String sai = Atividade
                .contrato(contrato)
                .entra(1)
                .realizar(integer -> null)
                .senao(integer -> null)
                .sai(integer -> "qualquer coisa");

        Assertions.assertEquals(contrato,sai);

    }

    @Test
    @DisplayName("Senao com Function<> nao nula, estado obstruido e Resultado da funcao Valido")
    void senaoComFunctionNaoNulaEstadoObsitruidoReslutadoDaFuncaoValido(){

        final Integer entrada = 1;

        final String sai = Atividade
                .contrato("0")
                .entra(entrada)
                .realizar(integer -> null)
                .senao(String::valueOf)
                .sai(integer -> "qualquer coisa");

        Assertions.assertEquals(String.valueOf(entrada),sai);

    }


    @Test
    @DisplayName("Senao com BiFunction<> nula")
    void senaoComBiFunctionNula(){

        Assertions.assertThrows(
                NullPointerException.class,
                () -> Atividade
                        .contrato("0")
                        .entra(1)
                        .senao((BiFunction<? super Integer,? super Integer, ? extends String>) null)
        );

    }

    @Test
    @DisplayName("Senao com BiFunction<> nao nula e estado fluivel")
    void senaoComBiFunctionNaoNulaEEstadoFluivel(){

        final String qualquerCoisa = "qualquer coisa";

        final String sai = Atividade
                .contrato("0")
                .entra(1)
                .realizar(integer -> integer * 2)
                .senao((integer, integer2) -> "5")
                .sai(integer -> qualquerCoisa);

        Assertions.assertEquals(qualquerCoisa,sai);
    }
    @Test
    @DisplayName("Senao com BiFunction<> nao nula, estado obstruido e Resultado da funcao Nula")
    void senaoComBiFunctionNaoNulaEstadoObstruidoEResultadoDaFuncaoNula(){

        final String contrato = "0";

        final String sai = Atividade
                .contrato(contrato)
                .entra(1)
                .realizar(integer -> null)
                .senao((integer, o) -> null)
                .sai(integer -> "qualquer coisa");

        Assertions.assertEquals(contrato,sai);

    }

    @Test
    @DisplayName("Senao com BiFunction<> nao nula, estado obstruido e Resultado da funcao Valido")
    void senaoComBiFunctionNaoNulaEstadoObsitruidoReslutadoDaFuncaoValido(){

        final Integer entrada = 1;

        final String sai = Atividade
                .contrato("0")
                .entra(entrada)
                .realizar(integer -> null)
                .senao((integer, o) -> String.valueOf(integer))
                .sai(integer -> "qualquer coisa");

        Assertions.assertEquals(String.valueOf(entrada),sai);

    }

}
