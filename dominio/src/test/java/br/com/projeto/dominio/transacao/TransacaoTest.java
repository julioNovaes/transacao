package br.com.projeto.dominio.transacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

class TransacaoTest {


    private static final Transacao TRANSACAO = Transacao.mock(LocalDateTime.now());
    private static final Periodo PERIODO = Periodo.mock();


    @Test
    @DisplayName("Crinado um transacao valida com dados validos")
    void transacaoValida() {

        Assertions.assertNotNull(TRANSACAO);

    }


    @Test
    @DisplayName("transacao Pertence Ao Periodo")
    void transacaoPertenceAoPeriodo() {


        Assertions.assertTrue(TRANSACAO.pertenceAoPeriodo(PERIODO));

    }


    @Test
    @DisplayName("transacao Compara transacao Maior")
    void transacaoComparaMaior() {


        Assertions.assertEquals(-1, TRANSACAO.compareTo(Transacao.mock(LocalDateTime.now())));

    }

    @Test
    @DisplayName("transacao Compara transacao Menor")
    void transacaoComparaMenor() {

        Transacao mock = Transacao
                .criar(Transacao.mock(LocalDateTime.now()))
                .comDataEfetiva(LocalDate.now().minusDays(2))
                .construir();

        Assertions.assertEquals(2, TRANSACAO.compareTo(mock));

    }
}