package br.com.projeto.dominio.transacao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MinhasDatas {


    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static LocalDate stringParaLocalDate(final String data) {
        return LocalDate.parse(data, DATE_TIME_FORMATTER);
    }

    public static String localDateParaString(final LocalDate localDate){

        return localDate.format(DATE_TIME_FORMATTER);
    }

}
