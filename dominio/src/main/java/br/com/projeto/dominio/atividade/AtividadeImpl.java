package br.com.projeto.dominio.atividade;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

class AtividadeImpl<A, B, C> implements Atividade<A, B, C> {

    private static final String FUNCAO_NAO_PODE_SER_NULA = "Funcao nao pode ser nula !!!!!!";

    private final A a;
    private final B b;
    private final C c;

    private final EstadoAtividade estadoAtivadade;

    private AtividadeImpl(final A a, final B b, final C c) {
        this(a, b, c, EstadoAtividade.FLUINDO);
    }

    private AtividadeImpl(final A a, final B b, final C c, final EstadoAtividade estadoAtivadade) {

        this.a = a;
        this.b = b;
        this.c = c;

        this.estadoAtivadade = estadoAtivadade;

    }

    @Override
    public <D> Atividade<A, D, C> realizar(final Function<? super B, D> funcao) {

        //Objects.requireNonNull(funcao, FUNCAO_NAO_PODE_SER_NULA);

        if (estadoAtivadade.naoFluivel()) return new AtividadeImpl<>(a, null, c, estadoAtivadade);

        final D d = funcao.apply(b);

        if (d == null) {

            return new AtividadeImpl<>(a, null, c, EstadoAtividade.OBSTRUCAO);

        }

        return construir(a, d, c);

    }

    @Override
    public <D> Atividade<A, D, C> realizar(final BiFunction<? super A, ? super B, D> funcao) {

        //Objects.requireNonNull(funcao, FUNCAO_NAO_PODE_SER_NULA);

        if (estadoAtivadade.naoFluivel()) return new AtividadeImpl<>(a, null, c, estadoAtivadade);

        final D d = funcao.apply(a, b);

        if (d == null) {

            return new AtividadeImpl<>(a, null, c, EstadoAtividade.OBSTRUCAO);

        }

        return construir(a, d, c);

    }

    @Override
    public Atividade<A, B, C> guarda(final Predicate<? super B> predicado) {

        Objects.requireNonNull(predicado, "Predicado nao pode ser nulo !!!!!!!!!!!");

        if (estadoAtivadade.finalizado()|| b == null) return this;

        final boolean test = predicado.test(b);

        return new AtividadeImpl<>(a,b,c,EstadoAtividade.valorDe(test));

    }

    @Override
    public Atividade<A, B, C> senao(final Function<? super A, ? extends C> funcao) {

        Objects.requireNonNull(funcao, FUNCAO_NAO_PODE_SER_NULA);

        if (estadoAtivadade.naoObstruido()) {
            return this;
        }
        final C issonaoeoccalculado = funcao.apply(a);

        if (issonaoeoccalculado == null) return new AtividadeImpl<>(a, b, c, EstadoAtividade.FIM);

        return new AtividadeImpl<>(a, b, issonaoeoccalculado, EstadoAtividade.FIM);

    }

    @Override
    public Atividade<A, B, C> senao(final BiFunction<? super A, ? super B, ? extends C> funcao) {

        Objects.requireNonNull(funcao, FUNCAO_NAO_PODE_SER_NULA);

        if (estadoAtivadade.naoObstruido()) {
            return this;
        }
        final C issonaoeoccalculado = funcao.apply(a, b);

        if (issonaoeoccalculado == null) return new AtividadeImpl<>(a, b, c, EstadoAtividade.FIM);

        return new AtividadeImpl<>(a, b, issonaoeoccalculado, EstadoAtividade.FIM);

    }

    @Override
    public C sai(final Function<? super B, ? extends C> funcao) {

        Objects.requireNonNull(funcao, FUNCAO_NAO_PODE_SER_NULA);

        if (estadoAtivadade.naoFluivel()) return c;

        final C issonaoeoccalculado = funcao.apply(b);

        if (issonaoeoccalculado == null) {
            return c;
        }

        return issonaoeoccalculado;

    }

    static <A, B, C> Atividade<A, B, C> construir(final A a, final B b, final C c) {

        return new AtividadeImpl<>(a, b, c);

    }


    private enum EstadoAtividade {

        /**
         * 0 FLUINDO
         * 1 OBSTRUCAO
         * 2 fim de flixo
         * <p>
         * realizar apenas quando o estado for 0
         * gurada nao ativado com estado 2
         * seano no estado 1
         * <p>
         * saida B nulo ou nao
         */

        FLUINDO, OBSTRUCAO, FIM;

        boolean naoFluivel() {
            return this != FLUINDO;
        }

        boolean naoObstruido() {
            return this != OBSTRUCAO;
        }

        boolean finalizado() {
            return this == FIM;
        }


        static EstadoAtividade valorDe(final boolean teste) {

            if (teste) {
                return EstadoAtividade.FLUINDO;
            }

            return EstadoAtividade.OBSTRUCAO;

        }

    }

}
