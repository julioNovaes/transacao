package br.com.projeto.dominio.conta;

import java.util.regex.Pattern;

public class ContaBancaria {

    private final String agencia;
    private final String conta;

    private static final Pattern padraoContaCorrente = Pattern.compile("^\\d{2,5}-?\\d$");
    private static final Pattern padraoAgencia = Pattern.compile("^\\d{4}$");


    public ContaBancaria(final String agencia, final String conta) {

        if (!agenciaCorrenteValida(agencia)) {
            throw new IllegalArgumentException("Agencia invalida");
        }
        if (!contaCorrenteValida(conta)) {
            throw new IllegalArgumentException("Conta invalida");
        }

        this.conta = conta;

        this.agencia = agencia;

    }

    public static boolean contaCorrenteValida(final String conta) {
        if (conta == null)
            return false;
        return padraoContaCorrente.matcher(conta).matches();
    }

    public static boolean agenciaCorrenteValida(final String agencia) {
        if (agencia == null)
            return false;
        return padraoAgencia.matcher(agencia).matches();
    }

    public String obterConta() {
        return conta;
    }

    public String obterAgencia() {
        return agencia;
    }

    public boolean encontrarContaBancaria(final String agencia, final String conta) {

        return this.agencia.equalsIgnoreCase(agencia) && this.conta.equalsIgnoreCase(conta);

    }


    public static ContaBancaria mock() {

        return new ContaBancaria("0000", "00000");

    }


}
