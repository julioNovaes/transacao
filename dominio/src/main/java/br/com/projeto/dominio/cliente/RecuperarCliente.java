package br.com.projeto.dominio.cliente;

import java.util.Optional;

@FunctionalInterface
public interface RecuperarCliente {

    Optional<Cliente> recuperar(final String cpf);
}
