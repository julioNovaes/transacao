package br.com.projeto.dominio.cliente;

import br.com.projeto.dominio.conta.ContaBancaria;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public class Cliente {

    private final SituacaoCliente situacaoCliente;
    private final String cpf;
    private final Collection<ContaBancaria> contasBancarias;

    private Cliente( final ClienteBuilder clienteBuilder ){
        situacaoCliente = clienteBuilder.situacaoCliente;
        cpf = clienteBuilder.cpf;
        contasBancarias = clienteBuilder.contasBancarias;
    }

    public static Cliente mock(){
        return construtor()
                .comSituacaoCliente( SituacaoCliente.ATIVO )
                .comCpf("37637316017")
                .comContasBancarias(Set.of(ContaBancaria.mock()))
                .construir();
    }

    public static boolean cpfValido(final String cpf) {

        final NumeroContribuinte numeroContribuinte = new NumeroContribuinte.PredicadoCPF();

        return numeroContribuinte.test(cpf);

    }

    public boolean habilitado() {

        return situacaoCliente == SituacaoCliente.ATIVO;

    }

    public String getCpf() {
        return cpf;
    }

    public Optional<ContaBancaria> contaBancaria(final String agencia, final String conta) {

        return contasBancarias
                .stream()
                .filter(contaBancaria -> contaBancaria
                        .encontrarContaBancaria(agencia, conta))
                .findFirst();

    }

    public static ClienteBuilder construtor(){
        return new ClienteBuilder();
    }

    public static ClienteBuilder construtor( final Cliente cliente ){
        return new ClienteBuilder( cliente );
    }

    public static class ClienteBuilder{

        private SituacaoCliente situacaoCliente;
        private String cpf;
        private Collection<ContaBancaria> contasBancarias;

        private ClienteBuilder(){}


        private ClienteBuilder( final Cliente cliente ){
            situacaoCliente = cliente.situacaoCliente;
            cpf = cliente.cpf;
            contasBancarias = cliente.contasBancarias;
        }

        public Cliente construir(){
            return new Cliente( this );
        }

        public ClienteBuilder comSituacaoCliente( final SituacaoCliente situacaoCliente ){
            this.situacaoCliente = situacaoCliente;
            return this;
        }

        public ClienteBuilder comCpf( final String cpf ){
            if( !Cliente.cpfValido( cpf ) ) throw new IllegalArgumentException("Cpf Informado Invalido");
            this.cpf = cpf;
            return this;
        }

        public ClienteBuilder comContasBancarias( final Collection<ContaBancaria> contasBancarias ){
            this.contasBancarias = contasBancarias;
            return this;
        }

    }

}
