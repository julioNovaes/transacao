package br.com.projeto.dominio.transacao;

public enum CategoriaTrasacao {

    TAXA(1),
    COMPRA(2),
    TRANSFERENCIA(3);

    private final int valor;

    CategoriaTrasacao(int valor) {
        this.valor = valor;
    }
}
