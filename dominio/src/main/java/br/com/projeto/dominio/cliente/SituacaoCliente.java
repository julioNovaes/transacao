package br.com.projeto.dominio.cliente;

public enum SituacaoCliente {


    PENDENTE(1),
    ATIVO(2),
    CANCELADO(3);

    private final int valor;


    SituacaoCliente(int valor) {
        this.valor = valor;
    }

}
