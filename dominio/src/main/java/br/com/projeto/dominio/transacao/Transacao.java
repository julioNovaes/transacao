package br.com.projeto.dominio.transacao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class Transacao implements Comparable<Transacao> {

    private final LocalDateTime dataHoraEfetiva;
    private final String autenticacao;
    private final BigDecimal valor;
    private final CategoriaTrasacao categoriaTrasacao;
    private final String descricao;

    private Transacao(final TransacaoBuilder transacaoBuilder) {

        dataHoraEfetiva = LocalDateTime.of(transacaoBuilder.dataEfetiva, transacaoBuilder.horaEfetiva);
        autenticacao = transacaoBuilder.autenticacao;
        valor = transacaoBuilder.valor;
        categoriaTrasacao = transacaoBuilder.categoriaTrasacao;
        descricao = transacaoBuilder.descricao;

    }

    public boolean pertenceAoPeriodo(final Periodo periodo) {

        return periodo.dataEntrePeriodo(dataHoraEfetiva.toLocalDate());

    }

    public static TransacaoBuilder criar() {

        return new TransacaoBuilder();
    }
    public static TransacaoBuilder criar(final Transacao transacao) {

        return new TransacaoBuilder(transacao);
    }

    public CategoriaTrasacao getCategoriaTrasacao() {
        return categoriaTrasacao;
    }

    public LocalDateTime getDataHoraEfetiva() {
        return dataHoraEfetiva;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public int compareTo(final Transacao transacao) {

        return dataHoraEfetiva.compareTo(transacao.dataHoraEfetiva);

    }

    /**
     * Vai cariar um Transacao cuja a data efetiva é a data atual
     * @return Transacao
     * @param now
     */
    public static Transacao mock(final LocalDateTime now) {

        return criar()
                .comAutenticao(UUID.randomUUID().toString())
                .comCategoria(CategoriaTrasacao.COMPRA)
                .comDescricao("Boneca inflavel")
                .comValor(BigDecimal.valueOf(100.00))
                .comDataEfetiva(now.toLocalDate())
                .comHoraEfetiva(now.toLocalTime())
                .construir();

    }
    public static Set<Transacao> setMock(LocalDateTime now){

        return new LinkedHashSet<>(
                Arrays.asList(
                        mock(now),
                        criar(mock(now))
                                .comDataEfetiva(now.toLocalDate().plusDays(1))
                                .comValor(BigDecimal.valueOf(25000))
                                .construir()
                )
        );

    }

    public static class TransacaoBuilder {

        private LocalDate dataEfetiva;
        private LocalTime horaEfetiva;
        private String autenticacao;
        private BigDecimal valor;
        private CategoriaTrasacao categoriaTrasacao;
        private String descricao;

        private TransacaoBuilder() {

        }

        private TransacaoBuilder(final Transacao transacao){

            dataEfetiva = transacao.dataHoraEfetiva.toLocalDate();
            horaEfetiva = transacao.dataHoraEfetiva.toLocalTime();
            autenticacao = transacao.autenticacao;
            valor = transacao.valor;
            categoriaTrasacao = transacao.categoriaTrasacao;
            descricao = transacao.descricao;

        }

        public TransacaoBuilder comDataEfetiva(final LocalDate dataEfetiva) {

            this.dataEfetiva = dataEfetiva;

            return this;

        }

        public TransacaoBuilder comHoraEfetiva(final LocalTime horaEfetiva) {

            this.horaEfetiva = horaEfetiva;

            return this;

        }

        public TransacaoBuilder comAutenticao(final String autenticacao) {

            this.autenticacao = autenticacao;

            return this;
        }

        public TransacaoBuilder comValor(final BigDecimal valor) {

            this.valor = valor;

            return this;

        }

        public TransacaoBuilder comCategoria(final CategoriaTrasacao categoriaTrasacao) {

            this.categoriaTrasacao = categoriaTrasacao;

            return this;
        }

        public TransacaoBuilder comDescricao(final String descricao) {

            this.descricao = descricao;

            return this;
        }

        public Transacao construir() {
            return new Transacao(this);
        }

    }

}
