package br.com.projeto.dominio.atividade;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * Para realizar um conjto especifico de ativiades
 * relcaionadaa a regra de negocio
 *
 * @param <A> Entrada Fixa
 * @param <B> Valor Volatil
 * @param <C> Saida Fixa
 *
 */
public interface Atividade<A,B,C> {

    static <C> Intermediario<C> contrato(final C c){
        Objects.requireNonNull(c, "C nao pode ser nulo !!!!!");
        return new Intermediario<>(c);}

    /**
     *
     * @param funcao
     * @param <D>
     * @return Atividade<A,D,C>
     */
    <D> Atividade<A,D,C> realizar( final Function<? super B, D> funcao );

    /**
     *
     * @param funcao
     * @param <D>
     * @return
     */
    <D> Atividade<A,D,C> realizar( final BiFunction<? super A, ? super B, D> funcao );

    /**
     *
     * @param predicado
     * @return
     */
    Atividade<A,B,C> guarda( final Predicate<? super B> predicado );

    /**
     *
     * @param funcao
     * @return
     */
    Atividade<A,B,C> senao( final Function<? super A, ? extends C> funcao );

    /**
     *
     * @param funcao
     * @return
     */
    Atividade<A,B,C> senao( final BiFunction<? super A, ? super B, ? extends C> funcao );

    /**
     *
     * @param funcao
     * @return
     */
    C sai( final Function<? super B, ? extends C> funcao );

    /**
     * Para encapsular a saida da ativadade ate que exista
     *
     * @param <C> tipo de saida da Ativadade
     */
    class Intermediario<C>{

        private final C c;

        private Intermediario(C c) {
            this.c = c;
        }

        /**
         *
         * @param a Entrada fixas
         * @param <A> Tipo generico da entrada fixa
         * @return Atividade<A,A,C> Com dos valores fixos
         */
         public <A> Atividade<A,A,C> entra( final A a ){
             Objects.requireNonNull(a, "A nao pode ser nulo !!!");
             return AtividadeImpl.construir(a,a,c);
         }

    }

}
