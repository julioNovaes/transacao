package br.com.projeto.dominio.transacao;

import br.com.projeto.dominio.conta.ContaBancaria;

import java.util.Collection;

@FunctionalInterface
public interface RecuperarTransacao {

   Collection<Transacao> obterTransacoes(final ContaBancaria contaBancaria);

}
