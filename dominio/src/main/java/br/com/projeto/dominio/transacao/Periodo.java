package br.com.projeto.dominio.transacao;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class Periodo {

    private final LocalDate dataInicio;
    private final LocalDate dataFim;

    public static Periodo entre(final String dataInicio, final String dataFim) {

        if (!dataValida(dataInicio)) {
            throw new IllegalArgumentException("Data inicio invalida!");
        }
        if (!dataValida(dataFim)) {
            throw new IllegalArgumentException("Data fim invalida!");
        }

        final LocalDate novaDataInicio = MinhasDatas.stringParaLocalDate(dataInicio);
        final LocalDate novaDataFim = MinhasDatas.stringParaLocalDate(dataFim);

        if ( periodoInvalido(novaDataInicio, novaDataFim) ) {
            throw new IllegalArgumentException("Periodos devem compreender dias positivos!");
        }

        return new Periodo(novaDataInicio, novaDataFim);

    }

    private Periodo(final LocalDate dataInicio, final LocalDate dataFim) {

        this.dataInicio = dataInicio;
        this.dataFim = dataFim;

    }

    private static boolean periodoInvalido(final LocalDate dataInicio, final LocalDate dataFim) {

        return dataInicio.isAfter(dataFim);

    }

    public static boolean dataValida(final String data) {

        if (data == null) {
            return false;
        }

        try {
            MinhasDatas.stringParaLocalDate(data);
        } catch (DateTimeParseException e) {
            return false;
        }

        return true;
    }



    boolean dataEntrePeriodo(final LocalDate data) {

        int valorDataInicio = data.compareTo(dataInicio);
        int valorDatafim = data.compareTo(dataFim);

        return valorDataInicio >= 0 && valorDatafim <= 0;
    }

    /**
     * O mock vai criar um periodo com  data inicio hoje e data fim amanha
     *
     * @return Periodo
     */
    public static Periodo mock() {

        final LocalDate hoje = LocalDate.now();

        return entre(MinhasDatas.localDateParaString(hoje), MinhasDatas.localDateParaString(hoje.plusDays(1)));

    }

}
