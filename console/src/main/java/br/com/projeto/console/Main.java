package br.com.projeto.console;

import br.com.projeto.aplicacao.apresentacao.ApresentarTransacao;
import br.com.projeto.aplicacao.apresentacao.RequisacaoEntrada;
import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.cliente.RecuperarCliente;
import br.com.projeto.dominio.transacao.RecuperarTransacao;
import br.com.projeto.dominio.transacao.Transacao;

import java.time.LocalDateTime;
import java.util.Optional;


public class Main {

    public static void main(String[] args) {

        final RecuperarCliente recupararCliente = cpf -> Optional.ofNullable(Cliente.mock());

        final LocalDateTime now = LocalDateTime.now();

        final RecuperarTransacao recuperarTransacao = contaBancaria -> Transacao.setMock(now);

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(now);

        final ApresentarTransacao apresentarTransacao = ApresentarTransacao.criar(recupararCliente, recuperarTransacao);

        apresentarTransacao.apresentar(requisacaoEntrada).repostaTransacao().forEach((categoriaTrasacao, saidaTransacaos) -> System.out.println(categoriaTrasacao));

    }

}
