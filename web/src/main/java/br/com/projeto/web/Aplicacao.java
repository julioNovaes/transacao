package br.com.projeto.web;

import br.com.projeto.aplicacao.apresentacao.ApresentarTransacao;
import br.com.projeto.aplicacao.apresentacao.RequisacaoEntrada;
import br.com.projeto.aplicacao.apresentacao.Resposta;
import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.cliente.RecuperarCliente;
import br.com.projeto.dominio.transacao.RecuperarTransacao;
import br.com.projeto.dominio.transacao.Transacao;
import com.google.gson.Gson;
import org.eclipse.jetty.http.HttpStatus;
import spark.Route;
import spark.Spark;

import java.time.LocalDateTime;
import java.util.Optional;

public class Aplicacao {

    public static final String URL_TRANSACOES_POR_CLIENTE_E_PERIODO = "clientes/:cpf/transacoes";
    public static final String URL_TESTE = "/transacoes";


    public static void main(String[] args) {

        Spark.port(8080);

        Spark.get(URL_TRANSACOES_POR_CLIENTE_E_PERIODO, (request, response) -> {

            final String cpf = request.params(":cpf");
            final String agencia = request.queryParams("agencia");
            final String conta = request.queryParams("conta");
            final String dataInico = request.queryParams("data_inicio");
            final String dataFim = request.queryParams("data_fim");

            final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
                @Override
                public String dataInicio() {
                    return dataInico;
                }

                @Override
                public String cpf() {
                    return cpf;
                }

                @Override
                public String dataFim() {
                    return dataFim;
                }

                @Override
                public String agencia() {
                    return agencia;
                }

                @Override
                public String conta() {
                    return conta;
                }
            };

            final LocalDateTime now = LocalDateTime.now();

            final RecuperarCliente recupararCliente = entrada -> Optional.ofNullable(Cliente.mock());

            final RecuperarTransacao recuperarTransacao = contaBancaria -> Transacao.setMock(now);

            final ApresentarTransacao apresentarTransacao = ApresentarTransacao.criar(recupararCliente, recuperarTransacao);

            final Resposta resposta = apresentarTransacao.apresentar(requisacaoEntrada);

            final Gson gson = new Gson();

            response.header("Content-type", "application/json");

            if (!resposta.respostasErros().isEmpty()) response.status(HttpStatus.BAD_REQUEST_400);

            return gson.toJson(resposta);

        });

    }
}
