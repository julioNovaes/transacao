package br.com.projeto.aplicacao.apresentacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class ApresentarTransacaoTest {

    private static final ApresentarTransacao APRESENTAR_TRANSACAO = new ApresentarTransacaoValidacaoProxy(null);

    @Test
    @DisplayName("Cenario CPF Invalido")
    public void cpfInvalido() {

        final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
            @Override
            public String cpf() {
                return "";
            }
        };

        AssertResposta
                .construtor()
                .comCampo(Campo.CPF)
                .comValor("")
                .comResposta(APRESENTAR_TRANSACAO.apresentar(requisacaoEntrada))
                .comStatus(Status.CAMPO_INVALIDO)
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cenario Data Inicio Invalida")
    public void dataInicioInvalida() {

        final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
            @Override
            public String dataInicio() {
                return "";
            }
        };

        AssertResposta
                .construtor()
                .comCampo(Campo.DATA_INICIO)
                .comValor("")
                .comResposta(APRESENTAR_TRANSACAO.apresentar(requisacaoEntrada))
                .comStatus(Status.CAMPO_INVALIDO)
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cenario Data Fim Invalida")
    public void dataFimInvalida() {

        final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
            @Override
            public String dataFim() {
                return "";
            }
        };

        AssertResposta
                .construtor()
                .comCampo(Campo.DATA_FIM)
                .comValor("")
                .comResposta(APRESENTAR_TRANSACAO.apresentar(requisacaoEntrada))
                .comStatus(Status.CAMPO_INVALIDO)
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cenario Agencia Invalida")
    public void agenciaInvalida() {

        final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
            @Override
            public String agencia() {
                return "";
            }
        };

        AssertResposta
                .construtor()
                .comCampo(Campo.AGENCIA)
                .comValor("")
                .comResposta(APRESENTAR_TRANSACAO.apresentar(requisacaoEntrada))
                .comStatus(Status.CAMPO_INVALIDO)
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cenario Conta Invalida")
    public void contaInvalida() {

        final RequisacaoEntrada requisacaoEntrada = new RequisacaoEntrada.RequisacaoEntradaImplementacao() {
            @Override
            public String conta() {
                return "";
            }
        };

        AssertResposta
                .construtor()
                .comCampo(Campo.CONTA)
                .comValor("")
                .comResposta(APRESENTAR_TRANSACAO.apresentar(requisacaoEntrada))
                .comStatus(Status.CAMPO_INVALIDO)
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cenario Apresentar Transações")
    public void apresentarTransacao() {

        final Resposta mock = RespostaParaTransacao.mock();

        final ApresentarTransacao apresentar = new ApresentarTransacaoValidacaoProxy(requisacaoEntrada -> mock);

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(LocalDateTime.now());

        final Resposta resposta = apresentar.apresentar(requisacaoEntrada);

        Assertions.assertEquals(mock,resposta);

    }


}
