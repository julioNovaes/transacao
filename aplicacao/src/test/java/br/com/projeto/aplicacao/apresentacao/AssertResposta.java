package br.com.projeto.aplicacao.apresentacao;

import org.junit.jupiter.api.Assertions;

import java.util.Collection;

public class AssertResposta {

   private final BuilderRespostaTest builderRespostaTest;

   private AssertResposta( final BuilderRespostaTest builderRespostaTest ){
      this.builderRespostaTest = builderRespostaTest;
   }
   void assertRespostaErro() {

      final Resposta resposta = builderRespostaTest.resposta;

      Assertions.assertNotNull( resposta );
      Assertions.assertTrue( resposta.repostaTransacao().isEmpty() );

      final Collection<SaidaErro> saidaErros = resposta.respostasErros();

      Assertions.assertFalse( saidaErros.isEmpty() );
      Assertions.assertEquals( 1, saidaErros.size() );

      final SaidaErro erro = saidaErros.iterator().next();

      Assertions.assertEquals( builderRespostaTest.status, erro.status() );
      Assertions.assertEquals( builderRespostaTest.campo, erro.campo() );
      Assertions.assertEquals( builderRespostaTest.valor, erro.valor() );

   }

   public static BuilderRespostaTest construtor(){
      return new BuilderRespostaTest();
   }

   static class BuilderRespostaTest{

      private Campo campo;
      private String valor;
      private Resposta resposta;
      private Status status;

      private BuilderRespostaTest(){}

      public BuilderRespostaTest comCampo( final Campo campo ){
         this.campo = campo;
         return this;
      }

      public BuilderRespostaTest comValor( final String valor ){
         this.valor = valor;
         return this;
      }

      public BuilderRespostaTest comResposta( final Resposta resposta ){
         this.resposta = resposta;
         return this;
      }

      public BuilderRespostaTest comStatus( final Status status ){
         this.status = status;
         return this;
      }

      public AssertResposta construir(){
         return new AssertResposta( this );
      }

   }

}
