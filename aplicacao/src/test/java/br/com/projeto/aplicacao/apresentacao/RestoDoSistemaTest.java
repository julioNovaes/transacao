package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.cliente.SituacaoCliente;
import br.com.projeto.dominio.transacao.CategoriaTrasacao;
import br.com.projeto.dominio.transacao.Transacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.*;

class RestoDoSistemaTest {

    private static final LocalDateTime DATA_PARA_TESTE = LocalDateTime.now();

    @Test
    @DisplayName("Cliente com cpf nao existe ")
    public void clienteNaoExiste() {

        final ApresentarTransacao restoDoSistema = ApresentarTransacao.criar(
                cpf -> Optional.empty(),
                contaBancaria -> {
                    throw new IllegalCallerException();
                });

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(DATA_PARA_TESTE);

        AssertResposta
                .construtor()
                .comCampo( Campo.CPF )
                .comValor( requisacaoEntrada.cpf() )
                .comResposta( restoDoSistema.apresentar(requisacaoEntrada) )
                .comStatus( Status.CLIENTE_NAO_ENCONTRADO )
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cliente nao habilitado ")
    public void clientenaoNaoHabilitado() {

        final ApresentarTransacao restoDoSistema = ApresentarTransacao.criar(cpf -> Optional.of(Cliente.construtor(Cliente
                .mock())
                .comSituacaoCliente(SituacaoCliente.CANCELADO)
                .construir()),
                contaBancaria -> {
                    throw new IllegalCallerException();
                });

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(DATA_PARA_TESTE);

        AssertResposta
                .construtor()
                .comCampo( Campo.CPF )
                .comValor( requisacaoEntrada.cpf() )
                .comResposta( restoDoSistema.apresentar(requisacaoEntrada) )
                .comStatus( Status.CLIENTE_NAO_HABILITADO )
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cliente sem conta")
    public void clienteSemConta() {

        final ApresentarTransacao restoDoSistema = ApresentarTransacao.criar(cpf -> Optional.of(Cliente.construtor(Cliente
                .mock())
                .comContasBancarias(Collections.emptyList())
                .construir()),
                contaBancaria -> {
                    throw new IllegalCallerException();
                });

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(DATA_PARA_TESTE);

        AssertResposta
                .construtor()
                .comCampo( Campo.CONTA_AGENCIA )
                .comValor( requisacaoEntrada.agenciaConta() )
                .comResposta( restoDoSistema.apresentar(requisacaoEntrada) )
                .comStatus( Status.CONTA_BANCARIA_NAO_ENCONTRADA )
                .construir()
                .assertRespostaErro();

    }

    @Test
    @DisplayName("Cliente sem trancacao para periodo")
    public void clienteSemTransacao() {

        final ApresentarTransacao restoDoSistema = ApresentarTransacao.criar(cpf -> Optional.of(Cliente.mock()), contaBancaria -> Collections.emptyList());

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(DATA_PARA_TESTE);

        AssertResposta
                .construtor()
                .comCampo( Campo.CONTA_AGENCIA )
                .comValor( requisacaoEntrada.agenciaConta() )
                .comResposta( restoDoSistema.apresentar(requisacaoEntrada) )
                .comStatus( Status.SEM_TRANSACOES )
                .construir()
                .assertRespostaErro();

    }


    @Test
    @DisplayName("Clinte com tudo certo e que vai voltar um mapa agrupaado de transacaoes")
    public void clienteComTransacoesValidas() {

        final LocalDateTime data = LocalDateTime.now();

        final Set<Transacao> transacoes = Transacao.setMock(data);

        final ApresentarTransacao restoDoSistema = ApresentarTransacao.criar(
                cpf -> Optional.of(Cliente.mock()),
                contaBancaria -> transacoes
        );

        final RequisacaoEntrada requisacaoEntrada = RequisacaoEntrada.mock(DATA_PARA_TESTE);

        final Resposta resposta = restoDoSistema.apresentar(requisacaoEntrada);

        Assertions.assertNotNull(resposta);
        Assertions.assertTrue(resposta.respostasErros().isEmpty());

        final Map<CategoriaTrasacao, Set<SaidaTransacao>> saidaTransacaoMap = resposta.repostaTransacao();

        Assertions.assertNotNull( saidaTransacaoMap );

        final Set<SaidaTransacao> respostaCategoriaCompra = saidaTransacaoMap.get(CategoriaTrasacao.COMPRA);

        Assertions.assertNotNull(respostaCategoriaCompra);
        Assertions.assertFalse( saidaTransacaoMap.containsKey(CategoriaTrasacao.TAXA) );
        Assertions.assertFalse( saidaTransacaoMap.containsKey(CategoriaTrasacao.TRANSFERENCIA) );

        Assertions.assertEquals( 2, respostaCategoriaCompra.size() );

        final Iterator<SaidaTransacao> iteratorSaidaTransacao = respostaCategoriaCompra.iterator();

        final SaidaTransacao saidaTransacaoComValor25000 = iteratorSaidaTransacao.next();
        final SaidaTransacao saidaTransacaoComValor100 = iteratorSaidaTransacao.next();

        final Iterator<Transacao> iteratorTransacoes = transacoes.iterator();

        final Transacao transacaoComValor100 = iteratorTransacoes.next();
        final Transacao transacaoComValor25000 = iteratorTransacoes.next();

        Assertions.assertEquals( transacaoComValor100.getDescricao(), saidaTransacaoComValor100.descricao() );
        Assertions.assertEquals( transacaoComValor25000.getDescricao(), saidaTransacaoComValor25000.descricao() );

        Assertions.assertEquals( transacaoComValor100.getValor(), saidaTransacaoComValor100.valor() );
        Assertions.assertEquals( transacaoComValor25000.getValor(), saidaTransacaoComValor25000.valor() );

        Assertions.assertTrue( saidaTransacaoComValor25000.dataEfetiva().isAfter( saidaTransacaoComValor100.dataEfetiva() ) );

    }

}