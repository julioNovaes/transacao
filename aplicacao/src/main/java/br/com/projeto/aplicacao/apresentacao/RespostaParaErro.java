package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.transacao.CategoriaTrasacao;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

final class RespostaParaErro implements Resposta {

    private final Collection<SaidaErro> saidaErros;

    RespostaParaErro(final Collection<SaidaErro> saidaErros) {
        this.saidaErros = saidaErros;
    }

    public static Resposta de(final SaidaErro saidaErro){

        return new RespostaParaErro(Collections.singletonList(saidaErro));

    }

    @Override
    public Map<CategoriaTrasacao, Set<SaidaTransacao>> repostaTransacao() {
        return Collections.emptyMap();
    }


    @Override
    public Collection<SaidaErro> respostasErros() {
        return saidaErros;
    }


}
