package br.com.projeto.aplicacao.apresentacao;

public interface SaidaErro {

    Status status();

    Campo campo();

    // valor que caracterisa erro
    String valor();

}
