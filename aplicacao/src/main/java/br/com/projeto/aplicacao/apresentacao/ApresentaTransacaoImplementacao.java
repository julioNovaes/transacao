package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.atividade.Atividade;
import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.cliente.RecuperarCliente;
import br.com.projeto.dominio.transacao.Periodo;
import br.com.projeto.dominio.transacao.RecuperarTransacao;
import br.com.projeto.dominio.transacao.Transacao;

import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

final class ApresentaTransacaoImplementacao implements ApresentarTransacao {

    private final RecuperarCliente recuperarCliente;

    private final RecuperarTransacao recuperarTransacao;

    ApresentaTransacaoImplementacao(final RecuperarCliente recuperarCliente, final RecuperarTransacao recuperarTransacao) {
        this.recuperarCliente = recuperarCliente;
        this.recuperarTransacao = recuperarTransacao;
    }

    @Override
    public Resposta apresentar(final RequisacaoEntrada requisacaoEntrada) {


        return Atividade

                .contrato( RespostaNula.criar() )

                .entra( requisacaoEntrada )

                .realizar( r -> recuperarCliente.recuperar(r.cpf()) )

                .guarda( Optional::isPresent )

                    .senao( r -> RespostaParaErro.de(
                            Status
                                    .CLIENTE_NAO_ENCONTRADO
                                    .erroBuilder()
                                    .comCampo(Campo.CPF)
                                    .comValor(r.cpf())
                                    .construir()
                    ) )

                .realizar( Optional::get )

                .guarda( Cliente::habilitado )

                    .senao(r -> RespostaParaErro.de(
                            Status
                                    .CLIENTE_NAO_HABILITADO
                                    .erroBuilder()
                                    .comCampo(Campo.CPF)
                                    .comValor(r.cpf())
                                    .construir()
                    ) )

                .realizar( (r, cliente) -> cliente.contaBancaria(r.agencia(),r.conta()) )

                .guarda( Optional::isPresent )

                    .senao( r -> RespostaParaErro.de(
                            Status
                                    .CONTA_BANCARIA_NAO_ENCONTRADA
                                    .erroBuilder()
                                    .comCampo(Campo.CONTA_AGENCIA)
                                    .comValor(r.agenciaConta())
                                    .construir()
                    ) )

                .realizar( Optional::get )

                .realizar( recuperarTransacao::obterTransacoes )

                .realizar( (r, transacaos) ->
                        transacaos
                                .stream()
                                .filter( transacao -> transacao.pertenceAoPeriodo(Periodo.entre(r.dataInicio(), r.dataFim())) )
                                .collect( Collectors.toUnmodifiableSet())
                )

                .guarda( t -> !t.isEmpty() )

                    .senao( r ->  RespostaParaErro.de(
                            Status
                                    .SEM_TRANSACOES
                                    .erroBuilder()
                                    .comCampo(Campo.CONTA_AGENCIA)
                                    .comValor(r.agenciaConta())
                                    .construir()
                    ) )

                .realizar( transacaos ->
                        transacaos
                                .stream()
                                .collect(
                                        Collectors.groupingBy(
                                                Transacao::getCategoriaTrasacao,
                                                Collectors.mapping(
                                                        SaidaTransacaoImplementacao::criar,
                                                        Collectors.toCollection(TreeSet::new)
                                                )
                                        )
                                )
                )

                .sai( RespostaParaTransacao::new );

    }

}
