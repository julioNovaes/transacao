package br.com.projeto.aplicacao.apresentacao;

public enum Status {

    CAMPO_INVALIDO( 1, "Campo informado invalido"),
    CLIENTE_NAO_ENCONTRADO( 2, "Cliente por CPF não foi encontrado"),
    CLIENTE_NAO_HABILITADO( 3, "Cliente não habilitado"),
    CONTA_BANCARIA_NAO_ENCONTRADA( 4, "Conta bancaria por agencia e conta não encontrada"),
    SEM_TRANSACOES( 5, "Não foram encontradas transações bancarias para o periodo informado");

    private final int valor;
    private final String mensagem;

    Status(final int valor, final String mensagem) {
        this.valor = valor;
        this.mensagem = mensagem;
    }

    SaidaErroImplementacao.ErroBuilder erroBuilder(){
        return SaidaErroImplementacao.construtor( this );
    }

}
