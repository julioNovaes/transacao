package br.com.projeto.aplicacao.apresentacao;

public enum Campo {

    CPF( "CPF" ),
    DATA_INICIO( "Data Inicio" ),
    DATA_FIM( "Data Fim" ),
    AGENCIA( "Agencia" ),
    CONTA( "Conta" ),
    CONTA_AGENCIA("Conta e Agencia");

    private final String titulo;

    Campo(final String titulo) {
        this.titulo = titulo;
    }

}
