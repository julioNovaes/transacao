package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.transacao.CategoriaTrasacao;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

class RespostaNula implements Resposta {

    private RespostaNula() {
    }

    @Override
    public Map<CategoriaTrasacao, Set<SaidaTransacao>> repostaTransacao() {
        return Map.of();
    }

    @Override
    public Collection<SaidaErro> respostasErros() {
        return Collections.emptyList();
    }

    static Resposta criar() {

        return new RespostaNula();
    }
}
