package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.transacao.CategoriaTrasacao;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface Resposta {

    Map<CategoriaTrasacao, Set<SaidaTransacao>> repostaTransacao();

    Collection<SaidaErro> respostasErros();



}
