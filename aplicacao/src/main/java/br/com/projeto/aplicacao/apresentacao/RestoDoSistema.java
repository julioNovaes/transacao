package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.cliente.RecuperarCliente;
import br.com.projeto.dominio.transacao.RecuperarTransacao;
import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.conta.ContaBancaria;
import br.com.projeto.dominio.transacao.CategoriaTrasacao;
import br.com.projeto.dominio.transacao.Periodo;
import br.com.projeto.dominio.transacao.Transacao;

import java.util.*;
import java.util.stream.Collectors;

final class RestoDoSistema implements ApresentarTransacao {

    private final RecuperarCliente recuperarCliente;

    private final RecuperarTransacao recuperarTransacao;

    RestoDoSistema(final RecuperarCliente recuperarCliente, RecuperarTransacao recuperarTransacao) {

        this.recuperarCliente = recuperarCliente;

        this.recuperarTransacao = recuperarTransacao;

    }

    @Override
    public Resposta apresentar(final RequisacaoEntrada requisacaoEntrada) {

        final String cpf = requisacaoEntrada.cpf();

        final Optional<Cliente> optionalCliente = recuperarCliente.recuperar(cpf);

        if (optionalCliente.isEmpty()) {

            return RespostaParaErro.de(
                    Status
                            .CLIENTE_NAO_ENCONTRADO
                            .erroBuilder()
                            .comCampo(Campo.CPF)
                            .comValor(cpf)
                            .construir()
            );

        }

        final Cliente cliente = optionalCliente.get();

        if (!cliente.habilitado()) {

            return RespostaParaErro.de(
                    Status
                            .CLIENTE_NAO_HABILITADO
                            .erroBuilder()
                            .comCampo(Campo.CPF)
                            .comValor(cpf)
                            .construir()
            );

        }

        final Optional<ContaBancaria> contaBancariaOptional = cliente.contaBancaria(requisacaoEntrada.agencia(), requisacaoEntrada.conta());

        if (contaBancariaOptional.isEmpty()) {

            return RespostaParaErro.de(
                    Status
                            .CONTA_BANCARIA_NAO_ENCONTRADA
                            .erroBuilder()
                            .comCampo(Campo.CONTA_AGENCIA)
                            .comValor(requisacaoEntrada.agenciaConta())
                            .construir()
            );

        }

        final Collection<Transacao> transacaos = recuperarTransacao.obterTransacoes(contaBancariaOptional.get());

        final Set<Transacao> transacaoList = transacaos
                .stream()
                .filter(transacao -> transacao.pertenceAoPeriodo(Periodo.entre(requisacaoEntrada.dataInicio(), requisacaoEntrada.dataFim())))
                .collect(Collectors.toUnmodifiableSet());

        if (transacaoList.isEmpty()) {
            return RespostaParaErro.de(
                    Status
                            .SEM_TRANSACOES
                            .erroBuilder()
                            .comCampo(Campo.CONTA_AGENCIA)
                            .comValor(requisacaoEntrada.agenciaConta())
                            .construir()
            );
        }

       final Map<CategoriaTrasacao, Set<SaidaTransacao>> mapTransacoes = transacaoList
                .stream()
                .collect(
                        Collectors.groupingBy(
                                Transacao::getCategoriaTrasacao,
                                Collectors.mapping(
                                        SaidaTransacaoImplementacao::criar,
                                        Collectors.toCollection(TreeSet::new)
                                )
                        )
                );

        return new RespostaParaTransacao(mapTransacoes);

    }


}
