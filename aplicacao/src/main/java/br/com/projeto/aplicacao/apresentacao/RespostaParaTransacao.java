package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.transacao.CategoriaTrasacao;

import java.util.*;

final class RespostaParaTransacao implements Resposta{

    private final Map<CategoriaTrasacao,Set<SaidaTransacao>> saidaTransacao;

    RespostaParaTransacao(final Map<CategoriaTrasacao, ? extends Set<SaidaTransacao>> saidaTransacao) {
        this.saidaTransacao = Collections.unmodifiableMap(saidaTransacao);
    }

    @Override
    public Map<CategoriaTrasacao, Set<SaidaTransacao>> repostaTransacao() {
        return saidaTransacao;
    }

    @Override
    public Collection<SaidaErro> respostasErros() {
        return Collections.emptyList();
    }


    public static Resposta mock(){

        return new RespostaParaTransacao(Map.of(CategoriaTrasacao.TAXA,Set.of(SaidaTransacaoImplementacao.mock())));

    }

}
