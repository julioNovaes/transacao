package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.transacao.Transacao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

class SaidaTransacaoImplementacao implements SaidaTransacao, Comparable<SaidaTransacaoImplementacao> {

    private final Transacao transacao;

    private SaidaTransacaoImplementacao(final Transacao transacao) {
        this.transacao = transacao;
    }

    @Override
    public String descricao() { return transacao.getDescricao(); }

    @Override
    public BigDecimal valor() { return transacao.getValor(); }

    @Override
    public LocalDateTime dataEfetiva() { return transacao.getDataHoraEfetiva();}

    @Override
    public int compareTo(SaidaTransacaoImplementacao saidaTransacaoImplementacao) {

        return -transacao.compareTo(saidaTransacaoImplementacao.transacao);

    }

    public static SaidaTransacao criar(final Transacao transacao){
        return new SaidaTransacaoImplementacao(transacao);
    }

    public static SaidaTransacaoImplementacao mock(){

        return new SaidaTransacaoImplementacao(Transacao.mock(LocalDateTime.now()));

    }

}
