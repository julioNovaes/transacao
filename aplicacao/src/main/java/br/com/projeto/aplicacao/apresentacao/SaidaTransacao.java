package br.com.projeto.aplicacao.apresentacao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface SaidaTransacao {

       String descricao();

       BigDecimal valor();

       LocalDateTime dataEfetiva();

}
