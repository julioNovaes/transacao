package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.cliente.RecuperarCliente;
import br.com.projeto.dominio.transacao.RecuperarTransacao;

public interface ApresentarTransacao {

    Resposta apresentar(final RequisacaoEntrada requisacaoEntrada);

    static ApresentarTransacao criar(final RecuperarCliente recupararCliente, final RecuperarTransacao recuperarTransacao){
        return new ApresentarTransacaoValidacaoProxy(
                new ApresentaTransacaoImplementacao(recupararCliente,recuperarTransacao)
        );
    }

}
