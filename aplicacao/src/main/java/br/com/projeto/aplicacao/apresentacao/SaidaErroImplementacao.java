package br.com.projeto.aplicacao.apresentacao;

class SaidaErroImplementacao implements SaidaErro {

    private final ErroBuilder erroBuilder;

    public static ErroBuilder construtor(final Status status){
        return new ErroBuilder(status);
    }

    private SaidaErroImplementacao(final ErroBuilder erroBuilder ){
        this.erroBuilder = erroBuilder;
    }

    static class ErroBuilder{

        private final Status status;
        
        private Campo campo;

        private String valor;

        private ErroBuilder( final Status status ){ this.status = status; }

        public ErroBuilder comCampo( final Campo campo ){
            this.campo = campo;
            return this;
        }

        public ErroBuilder comValor( final String valor ){
            this.valor = valor;
            return this;
        }

        public SaidaErro construir(){
            return new SaidaErroImplementacao( this );
        }

    }

    @Override
    public Status status() {
        return erroBuilder.status;
    }

    @Override
    public Campo campo() {
        return erroBuilder.campo;
    }

    @Override
    public String valor() {
        return erroBuilder.valor;
    }

}
