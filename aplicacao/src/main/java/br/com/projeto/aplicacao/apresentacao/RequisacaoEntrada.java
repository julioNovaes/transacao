package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.conta.ContaBancaria;
import br.com.projeto.dominio.transacao.MinhasDatas;

import java.time.LocalDateTime;

public interface RequisacaoEntrada {

    static RequisacaoEntrada mock(final LocalDateTime now) {
        return new RequisacaoEntradaImplementacao(now);
    }

    class RequisacaoEntradaImplementacao implements RequisacaoEntrada {

        private static final Cliente CLIENTE = Cliente.mock();
        private static final ContaBancaria CONTA_BANCARIA = ContaBancaria.mock();

        private final LocalDateTime localDateTime;

        RequisacaoEntradaImplementacao(final LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
        }

        protected RequisacaoEntradaImplementacao(){ localDateTime = LocalDateTime.now(); }

        // Esta é uma classe Esqueletica
        @Override
        public String cpf() {
            return CLIENTE.getCpf();
        }

        @Override
        public String dataInicio() {
            return MinhasDatas.localDateParaString(localDateTime.toLocalDate());
        }

        @Override
        public String dataFim() {
            return MinhasDatas.localDateParaString(localDateTime.toLocalDate().plusDays(1));
        }

        @Override
        public String agencia() {
            return CONTA_BANCARIA.obterAgencia();
        }

        @Override
        public String conta() {
            return CONTA_BANCARIA.obterConta();
        }

    }

    String cpf();

    String dataInicio();

    String dataFim();

    String agencia();

    String conta();

    default String agenciaConta() {

        return String.format("Agencia : %s, Conta : %s", agencia(), conta());

    }

}
