package br.com.projeto.aplicacao.apresentacao;

import br.com.projeto.dominio.cliente.Cliente;
import br.com.projeto.dominio.conta.ContaBancaria;
import br.com.projeto.dominio.transacao.Periodo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.Supplier;

final class ApresentarTransacaoValidacaoProxy implements ApresentarTransacao {

    private static final Status STATUS_CAMPO_INVALIDO = Status.CAMPO_INVALIDO;

    private final ApresentarTransacao apresentarTransacao;

    ApresentarTransacaoValidacaoProxy(final ApresentarTransacao apresentarTransacao) {
        this.apresentarTransacao = apresentarTransacao;
    }

    @Override
    public Resposta apresentar(final RequisacaoEntrada requisacaoEntrada) {

        return Validador
                .criar()
                .validar(
                        requisacaoEntrada.cpf(),
                        Cliente::cpfValido,
                        STATUS_CAMPO_INVALIDO.erroBuilder()
                                .comCampo(Campo.CPF)
                                .comValor(requisacaoEntrada.cpf())
                                ::construir
                )
                .validar(
                        requisacaoEntrada.dataInicio(),
                        Periodo::dataValida,
                        STATUS_CAMPO_INVALIDO.erroBuilder()
                                .comCampo(Campo.DATA_INICIO)
                                .comValor(requisacaoEntrada.dataInicio())
                                ::construir
                )
                .validar(
                        requisacaoEntrada.dataFim(),
                        Periodo::dataValida,
                        STATUS_CAMPO_INVALIDO.erroBuilder()
                                .comCampo(Campo.DATA_FIM)
                                .comValor(requisacaoEntrada.dataFim())
                                ::construir
                )
                .validar(
                        requisacaoEntrada.agencia(),
                        ContaBancaria::agenciaCorrenteValida,
                        STATUS_CAMPO_INVALIDO.erroBuilder()
                                .comCampo(Campo.AGENCIA)
                                .comValor(requisacaoEntrada.agencia())
                                ::construir
                )
                .validar(
                        requisacaoEntrada.conta(),
                        ContaBancaria::contaCorrenteValida,
                        STATUS_CAMPO_INVALIDO.erroBuilder()
                                .comCampo(Campo.CONTA)
                                .comValor(requisacaoEntrada.conta())
                                ::construir
                )
                .senaoResponda(() -> apresentarTransacao.apresentar(requisacaoEntrada));

        //Periodo.entre( requisacaoEntrada.dataInico(), requisacaoEntrada.dataFim() );

    }

    /***
     * caso do orElseGet
     * Supplier
     */
    private static final class Validador {

        private final Collection<SaidaErro> respostasErros;

        private Validador() {
            respostasErros = new ArrayList<>();
        }

        private static Validador criar() {
            return new Validador();
        }

        private <T> Validador validar(final T t, final Predicate<T> predicado, final Supplier<SaidaErro> supplier) {

            if (!predicado.test(t)) respostasErros.add(supplier.get());

            return this;

        }

        private Resposta senaoResponda(final Supplier<Resposta> outroCaso) {

            if (respostasErros.isEmpty()) return outroCaso.get();

            return new RespostaParaErro( respostasErros );

        }

    }

}
