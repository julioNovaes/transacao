
    1 - [Mapear] Entrar informaçao de cliente e sair cliente

    2 - [Fluxo de Predicado] Se cliente nao encontrado

        2.1 - [Fim de Fluxo] Apresentar Erro

    3 - [Fluxo de Predicado] Se cliente encontrado

        3.1 - [Fluxo de Predicado] Se cliente nao habilitado

            3.1.1 - [Fim de Fluxo] Apresentar Erro

        3.2 - [Fluxo de Predicado] Se cliente habilitado

            3.2.1 - [Mapear] Recuperar conta bancaria

            3.2.2 - [Fluxo de Predicado] Se conta bancaria nao encontrada

                3.2.2.1 - [Fim de Fluxo] Apresentar Erro

            3.2.3- [Fluxo de Predicado] Se conta bancaria encontrada

                3.2.3.1 - [Mapear] Listar Transaçoes

                3.2.3.2 - [Filtrar] Filtrar por periodo

                3.2.3.3- [Fluxo de Predicado] Se sem transacoes

                    3.2.3.3.1 - [Fim de Fluxo] Apresentar Erro

                3.2.3.4 - [Fluxo de Predicado] Se com transacoes

                    3.2.3.4.1 - [Fim de Fluxo] Apresentar Transacoes